/**
 * spark_src
 */
package com.xiongyingqi.cache;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;

import com.xiongyingqi.util.StringUtil;

import org.jivesoftware.spark.util.log.Log;
/**
 * @author XiongYingqi
 * @version 2013-6-26 下午10:05:51
 */
public class HashMapCache<K, V> extends LinkedHashMap<K, V> implements Serializable, Cache{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3306292439919162438L;
	
	
	private String cacheName;
	private Collection<CacheListener> cacheListeners;
	protected HashMapCache(){
	}
	protected HashMapCache(String cacheName){
		this.cacheName = cacheName;
	}
	
	/**
	 * String
	 * @return the cacheName
	 */
	public String getCacheName() {
		return cacheName;
	}
	
	/**
	 * @param cacheName the cacheName to set
	 */
	public void setCacheName(String cacheName) {
		this.cacheName = cacheName;
	}
	
	/**
	 * 当服务结束或其他情况需要清空缓存时将调用本方法
	 */
	public void notifyCacheDispose(){
		for (Iterator iterator = cacheListeners.iterator(); iterator.hasNext();) {
			CacheListener cacheListener = (CacheListener) iterator.next();
			try {
				cacheListener.dispose(this);
			} catch (Exception e) {
				Log.error(e);
			}
		}
	}
	
	/**
	 * 添加缓存监听
	 * @param cacheListener
	 * @throws CacheException
	 */
	public void addCacheListener(CacheListener cacheListener){
		if(cacheListener == null){
			try {
				throw new CacheException("Null of CacheListener");
			} catch (CacheException e) {
				Log.error(e);
			}
		}
		if(cacheListeners == null){
			cacheListeners = new LinkedHashSet<CacheListener>();
		}
		cacheListeners.add(cacheListener);
	}

	@Override
	public String toString() {
		return this.cacheName + ": " + super.toString();
	}
	
	@Override
	public int hashCode() {
		if(this.cacheName == null){
			this.cacheName = StringUtil.randomString();
		}
		return cacheName.hashCode();
	}
	@Override
	public boolean equals(Object o) {
		if(o != null && o instanceof HashSetCache){
			HashSetCache cache = (HashSetCache) o;
			return this.hashCode() == cache.hashCode();
		}
		return false;
	}
	
}
