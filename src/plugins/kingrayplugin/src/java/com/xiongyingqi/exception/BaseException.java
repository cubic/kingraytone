/**
 * spark_src
 */
package com.xiongyingqi.exception;

/**
 * 基础异常类
 * @author XiongYingqi
 * @version 2013-6-24 下午4:53:34
 */
public abstract class BaseException extends Exception{
	public BaseException(){
		
	}
	public BaseException(String message){
		super(message);
	}
	public BaseException(String message, Throwable throwable){
		super(message, throwable);
	}
	public BaseException(Throwable throwable){
		super(throwable);
	}
}
