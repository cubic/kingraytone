package com.kingray.spark.plugin.ui;
import org.jivesoftware.spark.util.log.Log;
import java.awt.Color;
import java.awt.Component;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.jivesoftware.spark.SparkManager;

import com.alee.extended.panel.GroupPanel;
import com.kingray.spark.plugin.service.MessageService;
import com.kingray.spark.plugin.service.Service;
import com.kingray.spark.plugin.service.ServiceThread;
import com.kingray.spark.plugin.service.UserService;
import com.kingray.spark.plugin.ui.component.ConversationItem;
import com.kingray.spark.plugin.vo.MessageVO;
import com.xiongyingqi.util.CalendarHelper;
import com.xiongyingqi.util.ComparatorHelper;
import com.xiongyingqi.util.StackTraceHelper;

/**
 * 会话主面板
 * 
 * @author XiongYingqi
 * 
 */
public class MessageHistoryPanel extends GroupPanel implements Service{
	private static final int REFRESH_DATE_LABEL_PERIOD = (int) (30 * CalendarHelper.MINUTE);
	private static final long serialVersionUID = 5901327705882097990L;
	private static Object LOCK = new Object(); // 锁
	private static MessageHistoryPanel singleton; // 单例
	private Map<String, ConversationItem> conversationItemMap;
	
	/**
	 * 消息服务类
	 */
	private MessageService messageService;

	/**
	 * 单例对象
	 * 
	 * @return
	 */
	public static MessageHistoryPanel getInstance() {
		synchronized (LOCK) {
			if (singleton == null) {
				singleton = new MessageHistoryPanel();
			}
		}
		return singleton;
	}

	private MessageHistoryPanel() {
		super(false); // 设置为纵向排列
		setPreferredWidth(this.getWidth());
		messageService = MessageService.getInstance();
		setOpaque(true);
		setBackground(new Color(245, 245, 245));
		conversationItemMap = new LinkedHashMap<String, ConversationItem>();
		beginRefreshDateLabelTimer(); // 开始计时器
//		initConversations();
	}
	
	/**
	 * 从数据库加载会话消息
	 * <br>2013-6-24 下午5:04:39
	 */
	private void initConversations(){
		Collection<MessageVO> conversations = messageService.getAllConversations();
		ComparatorHelper<MessageVO> comparatorHelper = new ComparatorHelper<MessageVO>();
		try {
			conversations = comparatorHelper.sortByDateTime(conversations, MessageVO.class.getDeclaredField("messageDateTime"), true);
		} catch (SecurityException e) {
			Log.error(e);
		} catch (NoSuchFieldException e) {
			Log.error(e);
		}
		for (Iterator iterator = conversations.iterator(); iterator.hasNext();) {
			MessageVO messageVO = (MessageVO) iterator.next();
			this.addConversation(messageVO);
		}
	}
	
	/**
	 * 开始刷新会话日期计时器
	 */
	private void beginRefreshDateLabelTimer() {
		RefreshConversationDateTimerTask conversationDateTimerTask = new RefreshConversationDateTimerTask();
		Timer timer = new Timer("ConversationDateTimer", true);
		timer.schedule(conversationDateTimerTask, REFRESH_DATE_LABEL_PERIOD,
				REFRESH_DATE_LABEL_PERIOD);
	}

	/**
	 * 刷新会话，使得本类能处理最近的会话
	 * 
	 * @param messageVO
	 */
	public void addConversation(MessageVO messageVO) {
		String currentUserName = SparkManager.getSessionManager().getUsername(); // 获取当前登录的用户
		String conversationUserName = messageVO.getMessageFrom().getUserName(); // 会话用户
		String messageTo = messageVO.getMessageTo().getUserName(); // 接收者

		if (conversationUserName == null
				|| (conversationUserName != null && currentUserName
						.equals(conversationUserName))) { // 如果信息的发送人为空或者发送人是自己，那么会话用户为接受人
			if (messageTo != null) { // 并且接受人也不为空
				conversationUserName = messageTo;
			} else {
				return; // 如果发送的目标不明确，那么就不添加
			}
		}

		ConversationItem conversationItem = conversationItemMap
				.get(conversationUserName);

		if (conversationItem == null) {
			conversationItem = this
					.addConversationItem(
							conversationUserName,
							messageVO.getMessageBody(),
							messageVO.getMessageDateTime(),
							UserService.getInstance().getRealName(
									conversationUserName));
			conversationItemMap.put(conversationUserName, conversationItem);
			this.add(0, conversationItem);
		} else {
			Component[] components = getComponents();
			for (int i = 0; i < components.length; i++) {
				Component component = components[i];
				if (component instanceof ConversationItem) {
					ConversationItem item = (ConversationItem) component;
					if (item.getConversationName().equals(conversationUserName)
							&& CalendarHelper.compareTwoDate(
									conversationItem.getMessageDateTime(),
									messageVO.getMessageDateTime())) { // 寻找单挑项目对象并且比较欲更改的日期是否比较新
						conversationItem.setMessageBody(messageVO
								.getMessageBody());
						conversationItem.setMessageDateTime(messageVO
								.getMessageDateTime());
						this.remove(item);
						this.add(0, conversationItem);
						conversationItemMap.put(conversationUserName,
								conversationItem);
						break;
					}
				}
			}
		}
		conversationItem.validate();
		this.validate();
	}

	/**
	 * 添加会话记录
	 * 
	 * @param messageFrom
	 * @param messageBody
	 * @param messageDate
	 * @return
	 */
	private ConversationItem addConversationItem(String messageFrom,
			String messageBody, Date messageDate, String conversationName) {
		ConversationItem conversationItem = new ConversationItem(
				this.getWidth());
		conversationItem.setMessageFrom(messageFrom);
		conversationItem.setMessageConversationName(conversationName);
		conversationItem.setMessageBody(messageBody);
		conversationItem.setMessageDateTime(messageDate);
		conversationItem.validate();
		return conversationItem;
	}

	/**
	 * 刷新日期显示，该方法被定时任务调用
	 */
	private void refreshConversationDate() {
		Component[] components = getComponents();
		for (int i = 0; i < components.length; i++) {
			Component component = components[i];
			if (component instanceof ConversationItem) {
				ConversationItem item = (ConversationItem) component;
				item.refreshDateLabel();
			}
		}
	}

	class RefreshConversationDateTimerTask extends TimerTask {
		@Override
		public void run() {
			MessageHistoryPanel.this.refreshConversationDate();
			if(!isServiceStarted){// 防止错过了刷新
				startService();
			}
		}

	}

	static boolean isServiceStarted;
	/**
	 * <br>2013-6-24 下午5:05:03
	 * @see com.kingray.spark.plugin.service.Service#startService()
	 */
	@Override
	public void startService() {
		try {
			initConversations();
			isServiceStarted = true;
		} catch (Exception e) {
			e.printStackTrace();
			Log.error(e);
			// TODO: handle exception
		}
	}
}
