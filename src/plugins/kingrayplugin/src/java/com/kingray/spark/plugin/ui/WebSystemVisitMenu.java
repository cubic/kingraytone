package com.kingray.spark.plugin.ui;

import java.awt.Desktop;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import org.jivesoftware.Spark;
import org.jivesoftware.spark.SparkManager;
import org.jivesoftware.spark.util.WinRegistry;

import com.jtattoo.plaf.JTattooUtilities;
import com.xiongyingqi.resource.Resource;
import com.xiongyingqi.util.BrowserHelper;
import com.xiongyingqi.util.FileHelper;
import com.xiongyingqi.util.KingrayResource;

import org.jivesoftware.spark.util.log.Log;
public class WebSystemVisitMenu extends JMenu {
	public WebSystemVisitMenu() {
		super(KingrayResource.getString("kingray.plugin.websystem"));
		init();
	}

	public void init() {
		// JMenu kingrayMenu = new JMenu(
		// KingrayResource.getString("kingray.plugin.websystem"));
		JMenuItem oaAndCMSMenuItem = new MyMenuItem(
				KingrayResource.getString("kingray.plugin.websystem.oa_cms"));
		oaAndCMSMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					browserURI(new URI(
									KingrayResource
											.getString("kingray.plugin.websystem.oa_cms.url")));
				} catch (URISyntaxException e3) {
					e3.printStackTrace();
				}
			}
		});

		JMenuItem itSupportMenuItem = new MyMenuItem(
				KingrayResource.getString("kingray.plugin.websystem.support"));
		itSupportMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
					try {
						browserURI(new URI(
										KingrayResource
												.getString("kingray.plugin.websystem.support.url")));
					} catch (URISyntaxException e3) {
						e3.printStackTrace();
					}
				}
		});

		this.add(oaAndCMSMenuItem);
		this.add(itSupportMenuItem);
	}

	private void browserURI(String uriStr){
		try {
			browserURI(new URI(uriStr));
		} catch (URISyntaxException e) {
			Log.error(e);
		}
	}
	
	
	private int tryTimes = 0;
	/**
	 * 浏览地址，默认使用chrome，如果chrome不存在，则使用系统默认浏览器 
	 * @param uri
	 */
	private void browserURI(final URI uri) {
		if (!Desktop.isDesktopSupported()) {
			return;
		}
		BrowserHelper.browserURI(uri);
		return;
//		if(JTattooUtilities.isWindows()){ // 如果是windows系统，则检查chrome
//			if(InstallerChromeFrame.getInstance(SparkManager.getMainWindow()).isVisible()){ // 如果安装等待界面打开了，那么关闭
//				InstallerChromeFrame.getInstance(SparkManager.getMainWindow()).dispose();
//			}
//			
//			String chromePath = null;
//			try {
//				chromePath = WinRegistry
//						.readString(
//								WinRegistry.HKEY_LOCAL_MACHINE,
//								"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths\\chrome.exe",
//								""); // 获取chrome浏览器
//				if(chromePath == null || "".equals(chromePath)){ // win8可能有点不同
//					chromePath = WinRegistry
//						.readString(
//								WinRegistry.HKEY_CURRENT_USER,
//								"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths\\chrome.exe",
//								""); // 获取chrome浏览器
//				}
////				RunInstallerFrameThread.hide();
//			} catch (IllegalArgumentException e) {
//				Log.error(e);
//			} catch (IllegalAccessException e) {
//				Log.error(e);
//			} catch (InvocationTargetException e) {
//				Log.error(e);
//			}
//			
//			if(chromePath != null && !"".equals(chromePath)){ // 安装了chrome则使用chrome打开
//				try {
//					Runtime.getRuntime().exec(chromePath + " " + uri); // 使用命令行打开
////				Desktop.getDesktop().open(new File(chromePath + " " + uri));
//				} catch (IOException e) {
//					Log.error(e);
//				}
//			} else { // 如果没有安装chrome则启动默认浏览器
////				RunInstallerFrameThread.show();
//				
//				Thread thread = new Thread(new Runnable() {
//					@Override
//					public void run() {
//						// 拷贝chrome并且运行安装
//						URL installerFileURL = Resource.getProgramResourcePath("chrome.exe");
//						String chromeInstallerPath = null;// chrome安装包位置
//						try {
//							chromeInstallerPath = new File(Spark.getSparkUserHome(), "chrome.exe").getCanonicalPath();
//							FileHelper.copyFile(installerFileURL, new File(chromeInstallerPath));
//						} catch (IOException e2) {
//							e2.printStackTrace();
//						}
//						if(chromeInstallerPath != null){
//							Process process = null;
//							try {
//								process = Runtime.getRuntime().exec(chromeInstallerPath);
//							} catch (IOException e1) {
//								e1.printStackTrace();
//							}
//							try {
//								int waitFor = process.waitFor(); // 等待安装完成
//							} catch (InterruptedException e) {
//								Log.error(e);
//							}
//							new File(chromeInstallerPath).delete(); // 删除安装文件
//							if(tryTimes++ < 5){ // 安装后测试是否成功
//								browserURI(uri);
//								return;
//							}
//						}
//					}
//				});
//				thread.start(); // 开始安装
//				
//				InstallerChromeFrame.getInstance(SparkManager.getMainWindow()).setVisible(true);
////				try {
////					Desktop.getDesktop().browse(uri);
////				} catch (IOException e) {
////					Log.error(e);
////				}
//			}
//		} else { // 如果是其他系统
//			try {
//				Desktop.getDesktop().browse(uri);
//			} catch (IOException e) {
//				Log.error(e);
//			}
//		}
		
	}
	
	class MyMenuItem extends JMenuItem{
		{
			this.setFont(new Font("微软雅黑", Font.PLAIN, 12));
		}
		MyMenuItem(){
			super();
		}
		MyMenuItem(String text){
			super(text);
		}
	}
	
	static class RunInstallerFrameThread extends Thread{
		static InstallerChromeFrame frame = InstallerChromeFrame.getInstance(SparkManager.getMainWindow());
		
		/**
		 * <br>2013-6-25 上午9:59:46
		 * @see java.lang.Thread#run()
		 */
		@Override
		public void run() {
			frame.setVisible(true);
		}
		
		public static void show(){
			if(!frame.isVisible()){
				RunInstallerFrameThread thread = new RunInstallerFrameThread();
				thread.start();
			}
		}
		
		public static void hide(){
			if(frame.isVisible()){
				frame.dispose();
			}
		}
		
	}
	
	public static void main(String[] args) {
		File file = new File("c:/a.exe");
		file.mkdirs();
	}
}
