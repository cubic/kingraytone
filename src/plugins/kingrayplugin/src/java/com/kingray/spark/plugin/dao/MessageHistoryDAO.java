package com.kingray.spark.plugin.dao;

import java.util.Collection;
import java.util.List;

import com.kingray.spark.plugin.vo.MessageVO;
import com.kingray.spark.plugin.vo.UserVO;

public interface MessageHistoryDAO {
	/**
	 * 插入多条消息
	 * @param messageHistories
	 */
	public void insertMessageHistories(Collection messageHistories);
	
	/**
	 * 插入单条历史消息
	 * @param messageVO
	 */
	public void insertMessageHistory(MessageVO messageVO);
	
	/**
	 * 根据用户名获取历史消息
	 * @param userName 用户名
	 * @param dateFrom 开始日期
	 * @param dateTo 结束日期
	 * @return List<MessageVO>
	 */
	public List<MessageVO> queryMessageHistoriesByUserName(String userName, String dateFrom, String dateTo);
	/**
	 * 加载所有用户
	 * @return
	 */
	public Collection<UserVO> getAllUser();
	/**
	 * 查找用户的真实姓名
	 * @param userName
	 * @return
	 */
	public String findUserRealName(String userName);
	/**
	 * 添加多个用户
	 * @param userVOs
	 */
	public void addUserVOs(Collection<UserVO> userVOs);

	/**
	 * 查询所有会话记录
	 * @return
	 */
	public Collection<MessageVO> getAllConversations();

	/**
	 * 获取总消息数
	 * @return 消息数
	 */
	public int getCoversationCount();

	/**
	 * 获取所有消息的预览
	 * @return Collection<MessageVO>
	 */
	public Collection<MessageVO> getAllConversationSummary();

	/**
	 * 根据关联用户获取历史消息
	 * <br>2013-6-25 下午4:45:46
	 * @param userName 用户名，可以是关联用户
	 * @return Collection<MessageVO>
	 */
	public Collection<MessageVO> queryMessageHistoriesByUserName(String userName);
	
	/**
	 * 根据关联用户获取历史消息
	 * <br>2013-6-25 下午4:45:46
	 * @param userName 用户名，可以是关联用户
	 * @param recordCount 消息数
	 * @param desc 是否倒序
	 * @return Collection<MessageVO>
	 */
	public Collection<MessageVO> queryMessageHistoriesByUserName(String userName, int recordCount, boolean desc);

	/**
	 * 根据关联用户和消息内容获取历史消息
	 * <br>2013-6-25 下午4:53:17
	 * @param relationUserName 关联用户
	 * @param messageBody 消息内容
	 * @return Collection<MessageVO>
	 */
	public Collection<MessageVO> searcheMessageHistoriesByMessageBody(
			String relationUserName, String messageBody);

}
