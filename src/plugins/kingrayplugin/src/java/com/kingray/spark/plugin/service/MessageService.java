package com.kingray.spark.plugin.service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;

import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.PacketCollector;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.filter.PacketIDFilter;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.spark.SessionManager;
import org.jivesoftware.spark.SparkManager;

import com.kingray.spark.packet.KingrayNameSpace;
import com.kingray.spark.plugin.KingrayPlugin;
import com.kingray.spark.plugin.dao.MessageHistoryDAO;
import com.kingray.spark.plugin.dao.impl.MessageHistoryDAOImpl;
import com.kingray.spark.plugin.handle.MessageHandler;
import com.kingray.spark.plugin.packet.MessageHistoryIQ;
import com.kingray.spark.plugin.packet.MessageHistoryResultIQ;
import com.kingray.spark.plugin.provider.MessageHistoryIQProvider;
import com.kingray.spark.plugin.vo.MessageVO;
import com.xiongyingqi.util.DomainHelper;
import com.xiongyingqi.util.StringHelper;

import org.jivesoftware.spark.util.log.Log;
/**
 * 消息服务，用户查询数据库与远程查询服务器的消息数据
 * @author KRXiongYingqi
 *
 */
public class MessageService implements Service{ //  PacketInterceptor,  
	private static final  KingrayNameSpace QUERY_MESSAGE = KingrayNameSpace.QUERY_MESSAGE;
	
	private MessageHistoryDAO messageHistoryDAO;
	private MessageHandler messageHandler;
	private XMPPConnection connection;
	
	private static MessageService singleton;
	private static final Object LOCK = new Object();

	public static MessageService getInstance() {
		synchronized (LOCK) {
			if (singleton == null) {
				singleton = new MessageService();
			}
		}
		return singleton;
	}
	
	
	private MessageService(){
//		System.out.println("ProviderManager.getInstance().getIQProviders() =========== " + ProviderManager.getInstance().getIQProviders());
		
		SessionManager sessionManager = SparkManager.getSessionManager();
		connection = sessionManager.getConnection();
		
//		connection.addPacketInterceptor(this, this); // 添加包过滤
//		connection.addPacketListener(this, this);
		
		
		messageHistoryDAO = MessageHistoryDAOImpl.getInstance(KingrayPlugin.getMessageHistoryPath());
		messageHandler = MessageHandler.getInstance();
		
		ProviderManager.getInstance().addIQProvider(QUERY_MESSAGE.getKey(), QUERY_MESSAGE.getValue(), new MessageHistoryIQProvider());
//		ServiceThread.addService(this);// 添加启动后的服务
	}
	
	/**
	 * 根据关联用户获取历史消息
	 * <br>2013-6-25 下午4:37:44
	 * @param relationUserName
	 * @return Collection<MessageVO>
	 */
	public Collection<MessageVO> queryMessageHistoriesByUserName(String relationUserName){
		relationUserName = DomainHelper.removeDomain(relationUserName);
		messageHandler.dispatcherMessageVos();
		return messageHistoryDAO.queryMessageHistoriesByUserName(relationUserName);
	}
	
	/**
	 * 根据消息内容查询消息
	 * <br>2013-6-26 上午10:09:11
	 * @param relationUserName
	 * @param messageBody
	 * @return Collection<MessageVO>
	 */
	public Collection<MessageVO> searcheMessageHistoriesByMessageBody(String relationUserName, String messageBody){
		relationUserName = DomainHelper.removeDomain(relationUserName);
//		messageHandler.dispatcherMessageVos();
		Collection<MessageVO> resultMessageVOs = new LinkedHashSet<MessageVO>(messageHistoryDAO.searcheMessageHistoriesByMessageBody(relationUserName, messageBody));
		if(messageHandler.getMessageVOCache().get(relationUserName) != null){
			resultMessageVOs.addAll(messageHandler.getMessageVOCache().get(relationUserName));
		}
		return messageHistoryDAO.searcheMessageHistoriesByMessageBody(relationUserName, messageBody);
	}
	
	/**
	 * 根据用户名查询消息数，并且限制返回行数
	 * <br>2013-6-26 上午10:07:40
	 * @param userName 用户名
	 * @param recordCount 返回行数
	 * @param desc 是否倒序
	 * @return Collection<MessageVO>
	 */
	public Collection<MessageVO> queryMessageHistoriesByUserName(
			String userName, int recordCount, boolean desc) {
		userName = DomainHelper.removeDomain(userName);
		Collection<MessageVO> resultMessageVOs = new LinkedHashSet<MessageVO>(messageHistoryDAO.queryMessageHistoriesByUserName(userName, recordCount, desc));
		if(messageHandler.getMessageVOCache().get(userName) != null){
			resultMessageVOs.addAll(messageHandler.getMessageVOCache().get(userName));
		}
		return resultMessageVOs;
	}
	
	
	/**
	 * 检查并同步历史数据到本地数据库
	 * <br>2013-6-26 上午10:10:06
	 */
	public void checkSynchronizeMessageHistory(){
		Collection<MessageVO> messageVOs = synchronizeMessageHistory();
		if(messageVOs != null && messageVOs.size() > 0){
			messageHistoryDAO.insertMessageHistories(messageVOs);
		}
	}
	
	/**
	 * 从服务器检查消息历史
	 */
	private Collection<MessageVO> synchronizeMessageHistory() {
		Packet packet = sendConversationCountCommand();// 总个数
//		if(!(packet instanceof MessageHistoryResultIQ)){
//			return null;
//		}
		
		MessageHistoryResultIQ messageHistoryResultIQ = (MessageHistoryResultIQ) packet;
		
		int count = messageHistoryDAO.getCoversationCount(); // 本地存储的数据
		if(messageHistoryResultIQ.getMessageCount() > count){ // 如果服务器存储的数据大于本地，那么进行取回处理
			Packet summaryPacket = sendConversationSummaryCommand();
			MessageHistoryResultIQ summaryResult = (MessageHistoryResultIQ) summaryPacket;
			Collection<MessageVO> serverMessageVOs = summaryResult.getMessageVOs(); // 服务器上存储的概要数据
			
			Collection<MessageVO> localMessageVOs = messageHistoryDAO.getAllConversationSummary(); // 本地存储的概要数据
//			// 检查消息缓存
//			if(messageHandler.getMessageVOCache() != null){
//				localMessageVOs.addAll(messageHandler.getMessageVOCache());
//			}
//			// 检查分组消息缓存
//			if(messageHandler.getGroupMessageVOCache() != null){
//				localMessageVOs.addAll(messageHandler.getGroupMessageVOCache());
//			}
			localMessageVOs.addAll(messageHandler.getAllChaches()); // 获取所有本地缓存数据
			
			serverMessageVOs.removeAll(localMessageVOs); // 移除已经存在的数据，能使用该方法的前提是MessageVO重写的hashCode()方法
			Collection<String> sendMessageIds = new  HashSet<String>();// 要获取的数据
			for (Iterator iterator = serverMessageVOs.iterator(); iterator
					.hasNext();) { // 添加待查询的任务
				MessageVO messageVO = (MessageVO) iterator.next();
				String leastSendMessageId = messageVO.getSendMessageId();
				if(StringHelper.notNullAndNotEmpty(leastSendMessageId)){
					sendMessageIds.add(leastSendMessageId);
				}
			}
			if(sendMessageIds.size() > 0){
				MessageHistoryResultIQ detailResultIQ = (MessageHistoryResultIQ) sendConversationDetailCommand(sendMessageIds);
				Collection<MessageVO> messageVOs = detailResultIQ.getMessageVOs();
				return messageVOs;
			}
		}
		return null;
	}
	
	
	private MessageService(XMPPConnection connection){
		this.connection = connection;
	}
	
	/**
	 * 查询所有的会话消息
	 * @return Collection<MessageVO>
	 */
	public Collection<MessageVO> getAllConversations(){
		return messageHistoryDAO.getAllConversations();
	}
	
	/**
	 * 发送查询消息数指令
	 */
	public Packet sendConversationCountCommand(){
		MessageHistoryIQ queryIq = new MessageHistoryIQ();
		queryIq.setActionType(MessageHistoryIQ.MessageHistoryActionType.QUERY_COUNT); // 设置业务类型
		connection.sendPacket(queryIq); // 发送指令
		
		PacketCollector packetCollector = connection.createPacketCollector(new PacketIDFilter(queryIq.getPacketID())); // 创建结果收集器
		Packet resultPacket = packetCollector.nextResult(); // 获取返回结果
		
		return resultPacket;
	}
	
	/**
	 * 发送查询消息概要
	 */
	public Packet sendConversationSummaryCommand(){
		MessageHistoryIQ queryIq = new MessageHistoryIQ();
		queryIq.setActionType(MessageHistoryIQ.MessageHistoryActionType.QUERY_SUMMARY);
		connection.sendPacket(queryIq);
		
		PacketCollector packetCollector = connection.createPacketCollector(new PacketIDFilter(queryIq.getPacketID())); // 创建结果收集器
		Packet resultPacket = packetCollector.nextResult(); // 获取返回结果
		
		return resultPacket;
	}
	
	/**
	 * 发送查询消息详情指令
	 * @param sendMessageIds 发送消息id
	 * @return Packet
	 */
	public Packet sendConversationDetailCommand(Collection<String> sendMessageIds){
		MessageHistoryIQ queryIq = new MessageHistoryIQ();
		queryIq.setActionType(MessageHistoryIQ.MessageHistoryActionType.QUERY_DETAIL);
		queryIq.setSendMessageIds(sendMessageIds); // 设置要查询的发送id
		connection.sendPacket(queryIq);
		
		PacketCollector packetCollector = connection.createPacketCollector(new PacketIDFilter(queryIq.getPacketID())); // 创建结果收集器
		Packet resultPacket = packetCollector.nextResult(); // 获取返回结果
		return resultPacket;
	}
	
	/**
	 * 发送查询消息详情
	 */
	public Packet sendConversationDetailCommand(){
		MessageHistoryIQ queryIq = new MessageHistoryIQ();
		queryIq.setActionType(MessageHistoryIQ.MessageHistoryActionType.QUERY_DETAIL);
		connection.sendPacket(queryIq);
		
		PacketCollector packetCollector = connection.createPacketCollector(new PacketIDFilter(queryIq.getPacketID())); // 创建结果收集器
		Packet resultPacket = packetCollector.nextResult(); // 获取返回结果
		
		return resultPacket;
	}
	
	@Override
	public void startService() {
		checkSynchronizeMessageHistory(); // 同步历史消息
	}
	
//	@Override
//	public void interceptPacket(Packet packet) {
//		System.out.println("interceptPacket packet ======== " + packet);
//	}
	
	public static void main(String[] args) {

        ProviderManager.getInstance().addIQProvider(QUERY_MESSAGE.getKey(), QUERY_MESSAGE.getValue(), new MessageHistoryIQProvider());

        XMPPConnection.DEBUG_ENABLED = true;

        final XMPPConnection connection = new XMPPConnection("10.188.199.169");
        try {
        	connection.connect();
        	connection.login("熊瑛琪", "111");
		} catch (XMPPException e) {
			Log.error(e);
		}
        MessageService messageService = new MessageService(connection);
        
        messageService.messageHistoryDAO = MessageHistoryDAOImpl.getInstance(KingrayPlugin.getMessageHistoryPath());
        messageService.messageHandler = MessageHandler.getInstance();
        
        messageService.checkSynchronizeMessageHistory();
//        messageService.sendConversationCountCommand();
//        messageService.sendConversationSummaryCommand();
//        messageService.sendConversationDetailCommand();
//        connection.addPacketListener(messageService, messageService);
//        new MessageService();
//        SipAccountPacket.getSipSettings(con);
//
//        System.out.println("HELLO");
	}
	


}
