package com.kingray.spark.plugin;

import java.awt.Font;
import java.io.IOException;

import javax.swing.JMenu;


import org.jivesoftware.MainWindow;
import org.jivesoftware.resource.SparkRes;
import org.jivesoftware.spark.SparkManager;
import org.jivesoftware.spark.Workspace;
import org.jivesoftware.spark.component.tabbedPane.SparkTabbedPane;
import org.jivesoftware.spark.plugin.Plugin;
import org.jivesoftware.spark.util.log.Log;

import com.kingray.spark.plugin.listener.KingrayMessageListener;
import com.kingray.spark.plugin.service.MessageService;
import com.kingray.spark.plugin.service.ServiceThread;
import com.kingray.spark.plugin.service.UserService;
import com.kingray.spark.plugin.ui.MessageHistory;
import com.kingray.spark.plugin.ui.MessageHistoryPanel;
import com.kingray.spark.plugin.ui.ScrolledMessageHistoryPanel;
import com.kingray.spark.plugin.ui.WebSystemVisitMenu;
import com.xiongyingqi.cache.CacheFactory;
import com.xiongyingqi.util.DomainHelper;
import com.xiongyingqi.util.KingrayResource;

/**
 * @author XiongYingqi
 *
 */
public class KingrayPlugin implements Plugin{
	/**
	 * 历史消息的存储路径
	 */
	private static String messageHistoryPath;
	/**
	 * 当前用户名
	 */
	public static String currentUserName;
	
	@Override
	public void initialize() {
		try {
			messageHistoryPath = SparkManager.getUserDirectory().getCanonicalPath();
		} catch (IOException e) {
			Log.error(e);
		}
		currentUserName = DomainHelper.removeDomain(SparkManager.getSessionManager().getUsername());
		
		addWebSystemMenu(); // 添加网络系统按钮
		addMessageHistoryTab(); // 添加会话tab
//		SparkManager.getMessageEventManager().addMessageEventRequestListener(new KingrayMessageListener());
//		SparkManager.getChatManager().addMessageFilter(new KingrayMessageListener()); // 添加消息过滤
		SparkManager.getChatManager().addGlobalMessageListener(new KingrayMessageListener());// 加入消息监听
		
		MessageHistory.getInstance();  // 初始化消息服务
		
		
		// 确保服务初始化
		ServiceThread.addService(UserService.getInstance());
		ServiceThread.addService(MessageService.getInstance());
		ServiceThread.addService(MessageHistoryPanel.getInstance());
		
		ServiceThread.startServices(); // 启动服务
	}
	/**
	 * 添加金瑞亿网络办公系统
	 */
	private void addWebSystemMenu() {
		final MainWindow mainWindow = SparkManager.getMainWindow();
		JMenu menu = new WebSystemVisitMenu();
		menu.setFont(new Font("微软雅黑", Font.PLAIN, 12));
        mainWindow.getJMenuBar().add(menu);
        mainWindow.validate(); // 刷新主面板，防止添加后不会马上显示
	}
	/**
	 * 添加历史消息tab
	 */
	private void addMessageHistoryTab(){
		Workspace workspace = SparkManager.getWorkspace();
        // Retrieve the Tabbed Pane from the WorkspaceUI.
        SparkTabbedPane tabbedPane = workspace.getWorkspacePane();
//        jScrollPane.setSize(workspace.getWidth(), workspace.getHeight());
//        jScrollPane.setPreferredSize(new Dimension(workspace.getWidth(), workspace.getHeight()));
//        jScrollPane.setCorner(key, corner)
        tabbedPane.addTab(KingrayResource.getString("kingray.plugin.conversation.tab.display"), SparkRes.getImageIcon(SparkRes.IMAGE_HISTORY), new ScrolledMessageHistoryPanel()); //message.ico
	}
	
	@Override
	public void shutdown() {
		CacheFactory.notifyCacheListenersDispose(); // 提醒缓存的清理
	}

	@Override
	public boolean canShutDown() {
		return false;
	}

	@Override
	public void uninstall() {
		
	}
	/**
	 * @return the messageHistoryPath
	 */
	public static String getMessageHistoryPath() {
		return messageHistoryPath;
	}
	
}
