/**
 * spark_src
 */
package com.xiongyingqi.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.LookAndFeel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import sun.swing.FilePane;

/**
 * @author XiongYingqi
 * @version 2013-6-27 上午10:29:24
 */
public class JSystemFileChooser extends JFileChooser {
	// LookAndFeel old = UIManager.getLookAndFeel();
	//
	// public JSystemFileChooser(){
	// super();
	// try {
	// UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	// } catch (ClassNotFoundException e) {
	// e.printStackTrace();
	// } catch (InstantiationException e) {
	// e.printStackTrace();
	// } catch (IllegalAccessException e) {
	// e.printStackTrace();
	// } catch (UnsupportedLookAndFeelException e) {
	// e.printStackTrace();
	// }
	// System.out.println(UIManager.getLookAndFeel());
	// }
	//
	// /**
	// * <br>2013-6-27 上午10:35:54
	// * @see javax.swing.JFileChooser#getSelectedFile()
	// */
	// @Override
	// public File getSelectedFile() {
	// try {
	// UIManager.setLookAndFeel(old);
	// } catch (UnsupportedLookAndFeelException ignored) {
	// }
	// return super.getSelectedFile();
	// }

	public void updateUI() {
		LookAndFeel old = UIManager.getLookAndFeel();
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Throwable ex) {
			old = null;
		}

		super.updateUI();

		if (old != null) {
			FilePane filePane = findFilePane(this);
			filePane.setViewType(FilePane.VIEWTYPE_DETAILS);
			filePane.setViewType(FilePane.VIEWTYPE_LIST);

			Color background = UIManager.getColor("Label.background");
			setBackground(background);
			setOpaque(true);

			try {
				UIManager.setLookAndFeel(old);
			} catch (UnsupportedLookAndFeelException ignored) {
			} // shouldn't get here
		}
	}

	private static FilePane findFilePane(Container parent) {
		for (Component comp : parent.getComponents()) {
			if (FilePane.class.isInstance(comp)) {
				return (FilePane) comp;
			}
			if (comp instanceof Container) {
				Container cont = (Container) comp;
				if (cont.getComponentCount() > 0) {
					FilePane found = findFilePane(cont);
					if (found != null) {
						return found;
					}
				}
			}
		}

		return null;
	}
}