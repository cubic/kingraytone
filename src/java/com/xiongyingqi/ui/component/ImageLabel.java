/**
 * RichTextTest
 */
package com.xiongyingqi.ui.component;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

import com.xiongyingqi.utils.FileHelper;

/**
 * 放置图片的标签，用于插入图片或获取图片对象的封装
 * @author 瑛琪 <a href="http://xiongyingqi.com">xiongyingqi.com</a>
 * @version 2013-9-4 下午3:28:26
 */
public class ImageLabel extends JLabel{
	/**
	 * 图片文件
	 */
	protected File iconFile;
	/**
	 * 图标
	 */
	protected Icon icon;
	
	public ImageLabel(Icon icon){
		super(icon);
		this.icon = icon;
	}
	
	public ImageLabel(File iconFile){
		this(new ImageIcon(iconFile.getAbsolutePath()));
		this.iconFile = iconFile;
	}

	/**
	 * File
	 * @return the iconFile
	 */
	public File getIconFile() {
		if(iconFile == null && icon instanceof BufferedImage){
			iconFile = FileHelper.readBufferImage((BufferedImage) icon);
		}
		return iconFile;
	}

	/**
	 * File
	 * @param iconFile the iconFile to set
	 */
	public void setIconFile(File iconFile) {
		this.iconFile = iconFile;
	}

	/**
	 * Icon
	 * @return the icon
	 */
	public Icon getIcon() {
		return icon;
	}

	/**
	 * Icon
	 * @param icon the icon to set
	 */
	public void setIcon(Icon icon) {
		this.icon = icon;
	}
	
}
