/**
 * RichTextTest
 */
package com.xiongyingqi.ui.component.editor;

/**
 * 编辑框内嵌对象类型枚举
 * @author 瑛琪 <a href="http://xiongyingqi.com">xiongyingqi.com</a>
 * @version 2013-9-2 下午3:26:20
 */
public enum EditorComponentType {
	/**
	 * 字符串类型
	 */
	STRING("string", EditorComponentString.class),
	/**
	 * 图片类型
	 */
	ICON("icon", EditorComponentIcon.class),
	/**
	 * 表情类型
	 */
	EMOTION("emotion", EditorComponentEmotion.class),
	/**
	 * UI组件类型
	 */
	COMPONENT("component", EditorComponentComponent.class);
	
	private String type;
	private Class<? extends EditorComponent> editorComponentClass;
	
	private EditorComponentType(String type, Class<? extends EditorComponent> editorComponentClass){
		this.type = type;
		this.editorComponentClass = editorComponentClass;
	}

	/**
	 * String
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * String
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Class<? extends EditorComponent>
	 * @return the editorComponentClass
	 */
	public Class<? extends EditorComponent> getEditorComponentClass() {
		return editorComponentClass;
	}

	/**
	 * Class<? extends EditorComponent>
	 * @param editorComponentClass the editorComponentClass to set
	 */
	public void setEditorComponentClass(Class<? extends EditorComponent> editorComponentClass) {
		this.editorComponentClass = editorComponentClass;
	}
}
