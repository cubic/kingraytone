/**
 * RichTextTest
 */
package com.xiongyingqi.ui.component.editor;

/**
 * @author 瑛琪 <a href="http://xiongyingqi.com">xiongyingqi.com</a>
 * @version 2013-9-3 下午4:02:12
 */
public class EditorComponentSupportException extends Exception{
	public EditorComponentSupportException(){
		super("The EditorComponent Not Implemented");
	}
	
	public EditorComponentSupportException(String message){
		super(message);
	}
	
	public EditorComponentSupportException(Throwable cause){
		super(cause);
	}
	
	public EditorComponentSupportException(String message, Throwable cause){
		super(message, cause);
	}
}
