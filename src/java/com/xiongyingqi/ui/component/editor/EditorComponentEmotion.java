/**
 * RichTextTest
 */
package com.xiongyingqi.ui.component.editor;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import com.xiongyingqi.emotion.Emotion;
import com.xiongyingqi.emotion.EmotionManager;
import com.xiongyingqi.utils.Base64;
import com.xiongyingqi.utils.FileHelper;
import com.xiongyingqi.utils.StringHelper;
import com.xiongyingqi.utils.StringUtil;

/**
 * 表情内嵌对象
 * @author 瑛琪 <a href="http://xiongyingqi.com">xiongyingqi.com</a>
 * @version 2013-9-2 下午3:29:24
 */
public class EditorComponentEmotion extends AbstractEditorComponent  {
	private static final Pattern PATTERN = Pattern.compile("<emotion[\\s]+?[^<>]+>");
	
	/**
	 * @param priority
	 */
	public EditorComponentEmotion() {
		super(0);
	}

	public Emotion emotion;
	
	/**
	 * <br>2013-9-2 下午3:29:45
	 * @see com.xiongyingqi.ui.component.editor.EditorComponent#getContent()
	 */
	@Override
	public Object getContent() {
		return emotion;
	}

	/**
	 * <br>2013-9-2 下午3:29:45
	 * @see com.xiongyingqi.ui.component.editor.EditorComponent#setContent(java.lang.Object)
	 */
	@Override
	public void setContent(Object content) {
		if(content instanceof Emotion){
			emotion = (Emotion) content;
		} else {
			throw new ClassCastException("类型错误");
		}
	}
	
	/**
	 * <br>2013-9-2 下午3:39:26
	 * @throws Exception 
	 * @see com.xiongyingqi.ui.component.editor.EditorComponent#insertContentToTextPane(javax.swing.JTextPane)
	 */
	@Override
	public void insertContentToTextPane(InputEditor inputEditor) throws Exception {
		if(emotion != null){
//			Icon icon = new ImageIcon(FileHelper.toURL(iconFile));
//			inputEditor.insertEmotion(emotionFile);
			inputEditor.insertIcon(emotion.getEmotionFile());
		} else {
			throw new Exception("插入的图标为空！");
		}
	}
	
	/**
	 * <br>2013-9-2 下午3:39:44
	 * @see com.xiongyingqi.ui.component.editor.EditorComponent#geType()
	 */
	@Override
	public EditorComponentType geType(){
		return EditorComponentType.EMOTION;
	}

	/**
	 * <br>2013-9-3 下午2:50:36
	 * @see com.xiongyingqi.ui.component.editor.EditorComponent#encodeToString()
	 */
	@Override
	public String encodeToString() throws Exception {
		String rs = "";
		if(emotion.isPrivate()){
			byte[] data = FileHelper.readFileToBytes(emotion.getEmotionFile());
			String encodeStr = Base64.encodeBytes(data);
			StringBuilder builder = new StringBuilder();
			builder.append("<emotion");
			builder.append(" src='");
			builder.append(encodeStr);
			builder.append("'");
			builder.append(" emotionId='");
			builder.append(emotion.getEmotionId());
			builder.append("'");
			builder.append(">");
			rs = builder.toString();
		} else {
			StringBuilder builder = new StringBuilder();
			builder.append("<emotion id='");
			builder.append(emotion.getId());
			builder.append("'>");
			rs = builder.toString();
		}
		return rs;
	}

	/**
	 * <br>2013-9-3 下午2:50:36
	 * @see com.xiongyingqi.ui.component.editor.EditorComponent#decodeFromString(java.lang.String)
	 */
	@Override
	public EditorComponent decodeFromString(String encoded) throws EditorComponentSupportException {
		Matcher matcherProperties = TAG_PROPERTIES_PATTERN.matcher(encoded);// 捕获属性字符串
		Map<String, String> map = new LinkedHashMap<String, String>();
		while (matcherProperties.find()) {
			String property = matcherProperties.group();
			Matcher matcherPropertyKey = TAG_PROPERTIES_KEY_PATTERN.matcher(property);
			Matcher matcherPropertyValue = TAG_PROPERTIES_VALUE_PATTERN.matcher(property);
			
			String key = null;
			String value = null;
			if(matcherPropertyKey.find()){
				key = matcherPropertyKey.group();
			}
			if(matcherPropertyValue.find()){
				value = matcherPropertyValue.group();
			}
			if(key != null && value != null){
				map.put(key, value);
			}
		}
		
		if(map.containsKey("id") ){
			Emotion emotion = EmotionManager.getInstance().getEmotionById(map.get("id"));
			EditorComponent editorComponent = new EditorComponentEmotion();
			editorComponent.setContent(emotion);
			return editorComponent;
		} else if(map.containsKey("src") ){
			String emotionId = StringHelper.UUID();
			if(map.containsKey("emotionId")){
				emotionId = map.get("emotionId");
			}
			
			Emotion emotion = EmotionManager.getInstance().getEmotionByEmotionId(emotionId);
			if(emotion == null){// 如果本地没有存储，则存储一下
				emotion = new Emotion();
				emotion.setEmotionId(emotionId);
				File emotionFile = Emotion.convertToPrivateEmotionFile(map.get("src"), emotion.getEmotionId());
				emotion.setEmotionFile(emotionFile);
			}
//			Emotion 
			EditorComponent editorComponent = new EditorComponentEmotion();
			editorComponent.setContent(emotion);
			return editorComponent;
		}
		return null;
	}

	/**
	 * <br>2013-9-18 上午11:13:10
	 * @see com.xiongyingqi.ui.component.editor.EditorComponent#getParsePattern()
	 */
	@Override
	public Pattern getParsePattern() throws EditorComponentSupportException {
		return PATTERN;
	}
}
