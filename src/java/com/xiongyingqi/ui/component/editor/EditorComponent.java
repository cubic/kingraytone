/**
 * RichTextTest
 */
package com.xiongyingqi.ui.component.editor;

import java.util.regex.Pattern;

import javax.swing.JTextPane;

/**
 * 编辑框内嵌对象
 * 
 * @author 瑛琪 <a href="http://xiongyingqi.com">xiongyingqi.com</a>
 * @version 2013-9-2 下午3:22:54
 */
public interface EditorComponent {

	/**
	 * 作为解释器角色的解码优先级 <br>
	 * 2013-9-18 上午10:08:16
	 * 
	 * @return
	 */
	public int getDecodePriority();

	/**
	 * 获取组件内容 <br>
	 * 2013-9-2 下午3:29:50
	 * 
	 * @return Object
	 */
	public Object getContent();

	/**
	 * 设置内嵌对象的插入内容 <br>
	 * 2013-9-2 下午3:39:08
	 * 
	 * @param content
	 *            插入的内容
	 */
	public void setContent(Object content);

	/**
	 * 将内容插入到编辑框内 <br>
	 * 2013-9-2 下午3:30:00
	 * 
	 * @param inputEditor
	 *            要插入的编辑框
	 * @throws Exception
	 */
	public void insertContentToTextPane(InputEditor inputEditor) throws Exception;

	/**
	 * 将内嵌对象压缩成字符串 <br>
	 * 2013-9-3 下午12:01:46
	 * 
	 * @return
	 */
	public String encodeToString() throws Exception;

	/**
	 * 获取内嵌对象类型 <br>
	 * 2013-9-2 下午3:41:21
	 * 
	 * @return com.xiongyingqi.ui.component.editor.EditorComponentType
	 * @see com.xiongyingqi.ui.component.editor.EditorComponentType
	 */
	public EditorComponentType geType();

	/**
	 * 将指定的字符串进行解码 <br>
	 * 2013-9-18 上午10:39:25
	 * 
	 * @param encoded
	 *            要解码的字符串
	 * @return 解码后的EditorComponent对象
	 * @throws EditorComponentSupportException
	 *             如果未实现，则抛出错误
	 */
	public EditorComponent decodeFromString(String encoded) throws EditorComponentSupportException;

	/**
	 * 获取每个EditorComponent对象（解释器）的解码的正则表达式<br>
	 * 如果解释器存在有解释的内容，则返回捕获自己关注的内容的正则表达式；<br>
	 * 如果解释器是字符串类型（意思是直接显示字符串的内嵌对象，目前只有EditorComponentString类型是这样的），则返回null；<br>
	 * 如果解释器没有具体的实现（可能是保留类型，如EditorComponentComponent），则抛出异常；<br>
	 * <br>
	 * 2013-9-18 下午12:12:58
	 * 
	 * @return 查找标签的正则表达式，如果是字符串类型，则返回空
	 * @throws EditorComponentSupportException
	 *             如果未实现，则抛出错误
	 */
	public Pattern getParsePattern() throws EditorComponentSupportException;

}
