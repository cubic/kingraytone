/**
 * RichTextTest
 */
package com.xiongyingqi.ui.component.editor;

/**
 * @author 瑛琪 <a href="http://xiongyingqi.com">xiongyingqi.com</a>
 * @version 2013-9-3 下午4:02:12
 */
public class IconPermissionException extends Exception{
	public IconPermissionException(){
		super("IconSecurityPolicyErro");
	}
	
	public IconPermissionException(String message){
		super(message);
	}
	
	public IconPermissionException(Throwable cause){
		super(cause);
	}
	
	public IconPermissionException(String message, Throwable cause){
		super(message, cause);
	}
}
