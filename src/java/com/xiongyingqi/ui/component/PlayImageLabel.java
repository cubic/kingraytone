/**
 * RichTextTest
 */
package com.xiongyingqi.ui.component;

import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.lang.reflect.InvocationTargetException;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;

/**
 * 放置图片的标签，可用于扩充操作
 * @author 瑛琪 <a href="http://xiongyingqi.com">xiongyingqi.com</a>
 * @version 2013-8-30 上午10:45:19
 */
public class PlayImageLabel extends ImageLabel {
	
	public PlayImageLabel(Icon icon){
		super(icon);
	}
	
	public PlayImageLabel(File iconFile){
		super(iconFile);
	}
	
//	/**
//	 * <br>2013-8-30 上午10:42:56
//	 * @see javax.swing.JComponent#paint(java.awt.Graphics)
//	 */
//	@Override
//	public void paint(Graphics g) {
//		g.drawImage(icon.getImage(), 0, 0, null);
//	}

}
