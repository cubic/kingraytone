/**
 * spark_src
 */
package com.xiongyingqi.resources;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.jivesoftware.resource.SparkRes;

/**
 * @author XiongYingqi
 * @version 2013-6-24 下午7:02:59
 */
public class Resource {
	// private static String RESOURCE_FILE_NAME;
	private static String PROGRAM_PATH = "programs/";
	private static String IMAGES_PATH = "images/";

	public static Map<String, String> resources;

	static {
		// RESOURCE_FILE_NAME = Resource.class.getSimpleName();

		/**
		 * if(resources == null){ resources = new HashMap<String, String>();
		 * String path =
		 * Resource.class.getClassLoader().getResource(PROGRAM_PATH).getFile();
		 * System.out.println("path ========== " + path); File pathFile = new
		 * File(path); System.out.println("pathFile ================ " +
		 * pathFile); FilenameFilter filenameFilter = new FilenameFilter() {
		 * 
		 * @Override public boolean accept(File dir, String name) { //
		 *           System.out.println(name); if(name.endsWith("class")){
		 *           return false; } return true; } }; File[] files =
		 *           pathFile.listFiles(); if(files != null){ for (int i = 0; i
		 *           < files.length; i++) { File resource = files[i];
		 *           System.out.println("resource ============ " + resource);
		 *           String resourceName = resource.getName(); resourceName =
		 *           resourceName.substring(0,
		 *           resourceName.lastIndexOf(".")).toLowerCase();
		 *           resources.put(resourceName, PROGRAM_PATH +
		 *           resource.getName()); //
		 *           System.out.println(resource.getName()); } } }
		 */
	}

	// public static String getResourcePath(String key){
	// String path = resources.get(key);
	// if(path != null && !"".equals(path.trim())){
	// return Resource.class.getClassLoader().getResource(path).getFile();
	// }
	// return null;
	// }
	public static URL getProgramResourcePath(String file) {
		return Resource.class.getClassLoader().getResource(PROGRAM_PATH + file);
	}

	public static URL getImageResourcePath(String file) {
		return Resource.class.getClassLoader().getResource(IMAGES_PATH + file);
	}

	public static void main(String[] args) {
		System.out.println(Resource.class.getClassLoader());
		System.out.println(Resource.class.getClassLoader().getResource(
				PROGRAM_PATH + "chrome.exe"));
		// System.out.println(Resource.class.getSimpleName());
		// System.out.println(Resource.class.getClassLoader().loadClass(Resource.class.getName()));
		// Class clazz =
		// Resource.class.getClassLoader().loadClass(Resource.class.getName());
		// URL url =
		// Resource.class.getClassLoader().getSystemResource(Resource.class.getSimpleName());
		// System.out.println(url);
	}

}
