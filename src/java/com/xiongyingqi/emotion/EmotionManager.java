/**
 * spark_src
 */
package com.xiongyingqi.emotion;


import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.jivesoftware.Spark;

import com.xiongyingqi.utils.ComparatorHelper;
import com.xiongyingqi.utils.EntityHelper;
import com.xiongyingqi.utils.FileHelper;
import com.xiongyingqi.utils.StringHelper;
import com.xiongyingqi.utils.ThreadPool;

/**
 * @author 瑛琪 <a href="http://xiongyingqi.com">xiongyingqi.com</a>
 * @version 2013-9-4 下午4:23:37
 */
public class EmotionManager implements EmotionListener{
	/**
	 * 存储公共表情图片的目录
	 */
	public static final File EMOTION_FOLDER = new File(Spark.getBinDirectory().getParent(),
			"emotions").getAbsoluteFile();
	
	private static final ComparatorHelper<Emotion> COMPARATOR_HELPER = new ComparatorHelper<Emotion>();
	
	/**
	 * 所有的表情对象监听
	 */
	private static final List<EmotionListener> EMOTION_LISTENERS = new ArrayList<EmotionListener>();
	
	/**
	 * 存储私有表情图片的目录
	 */
	public static final File EMOTION_PRIVATE_FOLDER = new File(EMOTION_FOLDER,
			"private").getAbsoluteFile();
	
//	private static Set<Emotion> emotions;
	private Set<Emotion> publicEmotions;
	private Set<Emotion> privatEmotions;
	private static final Object LOCK = new Object();
	private static EmotionManager singleton;
	
	private EmotionManager(){
		publicEmotions = new LinkedHashSet<Emotion>();
		privatEmotions = new LinkedHashSet<Emotion>();
		loadPublicEmotions();
		loadPrivateEmotions();
	}
	
	public static EmotionManager getInstance(){
		synchronized (LOCK) {
			if(singleton == null){
				singleton = new EmotionManager();
			}
		}
		return singleton;
	}
	
	public void addEmotionListener(EmotionListener emotionListener){
		EMOTION_LISTENERS.add(emotionListener);
	}
	
	/**
	 * 在指定目录加载表情
	 * <br>2013-9-6 下午3:31:09
	 * @param folder
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private Collection<Emotion> loadEmotionsInFolder(File folder){
		File[] files = FileHelper.listFilesBySuffix(folder, Emotion.EMOTION_INFO_FILE_SUFFIX);
//		if(files == null){
//			return null;
//		}
		Collection<Emotion> emotions = new ArrayList<Emotion>();
		try {
			for (int i = 0; i < files.length; i++) {
				File file = files[i];
				try {
					Emotion emotion = (Emotion) FileHelper.unSerializeObjectFromFile(file);
					emotions.add(emotion);
				} catch (Exception e) {
				}
			}
		} catch (Exception e) {
		}
		try {
			emotions = COMPARATOR_HELPER.sortByString(emotions, Emotion.class.getDeclaredField("id"), true);
			for (Iterator iterator = emotions.iterator(); iterator.hasNext();) {
				Emotion emotion = (Emotion) iterator.next();
			}
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		}
		return emotions;
	}
	
	/**
	 * 加载本地的所有共享表情对象
	 * <br>2013-9-6 下午3:31:20
	 * @return
	 */
	public Collection<Emotion> loadPublicEmotions(){
		publicEmotions.clear();
		publicEmotions.addAll(loadEmotionsInFolder(EMOTION_FOLDER));
		return publicEmotions;
	}
	
	/**
	 * 加载本地的所有私有表情对象
	 * <br>2013-9-6 下午3:31:33
	 * @return
	 */
	public Collection<Emotion> loadPrivateEmotions(){
		privatEmotions.clear();
		privatEmotions.addAll(loadEmotionsInFolder(EMOTION_PRIVATE_FOLDER));
		return privatEmotions;
	}
	
	/**
	 * 加载所有表情对象
	 * <br>2013-9-6 下午6:42:33
	 * @return Collection<Emotion>
	 */
	public void loadAllEmotions(){
		synchronized(LOCK){
			privatEmotions.clear();
			publicEmotions.clear();
			privatEmotions.addAll(loadPrivateEmotions());
			publicEmotions.addAll(loadPublicEmotions());
		}
	}
	
	/**
	 * 新增表情对象
	 * <br>2013-9-6 下午3:39:31
	 * @param emotionFile
	 * @param emotionString
	 * @param isPrivate
	 */
	public void addEmotion(File emotionFile, String emotionString, boolean isPrivate){
		Collection<Emotion> emotions = listDirectoryEmotions(emotionFile, emotionString, isPrivate);
		addEmotion(emotions);
	}
	
	private Collection<Emotion> listDirectoryEmotions(File emotionFile, String emotionString, boolean isPrivate){
		Collection<Emotion> emotions = new ArrayList<Emotion>();
		if(emotionFile.exists()){
			if(!emotionFile.isDirectory()){
				if(!FileHelper.isImageFile(emotionFile) || !FileHelper.checkFileSizeLessThen(emotionFile, 500 * FileHelper.KB)){
					return emotions;
				}
				Emotion emotion = new Emotion();
				
				String emotionId = StringHelper.UUID();
				
				File targetFile = new File(isPrivate ? EMOTION_PRIVATE_FOLDER : EMOTION_FOLDER, emotionId + Emotion.EMOTION_IMAGE_FILE_SUFFIX);
				FileHelper.copyFile(emotionFile, targetFile);
				
				emotion.setEmotionFile(targetFile);
				emotion.setEmotionId(emotionId);
				emotion.setEmotionString(emotionString);
				emotion.setPrivate(isPrivate);
				if(isPrivate){// 如果是私有的表情，则使用UUID作为ID识别
					emotion.setId(emotionId);
					try {
						emotion.store();// 直接存储
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				emotions.add(emotion);
			} else {// 如果是添加的目录
				File[] files = emotionFile.listFiles();
				for (int i = 0; i < files.length; i++) {
					File file = files[i];
					emotions.addAll(listDirectoryEmotions(file, emotionString, isPrivate));
				}
			}
		}
		return emotions;
	}
	
	/**
	 * 新增表情对象
	 * <br>2013-9-6 下午3:38:11
	 * @param emotion
	 */
	public void addEmotion(Collection<Emotion> emotions){
		String result  = null;
		Collection<Emotion> emotionsToAdd = new ArrayList<Emotion>();
		for (Iterator iterator = emotions.iterator(); iterator.hasNext();) {
			Emotion emotionToAdd = (Emotion) iterator.next();
			if(emotionToAdd.isPrivate()){
				privatEmotions.add(emotionToAdd);
			} else {
				emotionsToAdd.add(emotionToAdd);
			}
		}
		EmotionService.getInstance().addEmotion(emotionsToAdd);
	}
	
	public void synchonizeEmotionsInthread(){
		try {
			ThreadPool.invoke(this, this.getClass().getDeclaredMethod("synchronizeEmotions"));
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 和服务器同步表情对象
	 * <br>2013-9-6 下午3:36:04
	 */
	public void synchronizeEmotions(){
		Collection<Emotion> emotions = loadPublicEmotions();
		Collection<Emotion> receivedEmotions = EmotionService.getInstance().synchroEmotions(emotions);// 从服务器返回的表情对象
		if(receivedEmotions != null){
			publicEmotions.addAll(receivedEmotions);
		}
	}
	
	/**
	 * <br>2013-9-7 下午3:01:38
	 * @see com.xiongyingqi.emotion.EmotionListener#emotionAdded(com.xiongyingqi.emotion.Emotion)
	 */
	@Override
	public void emotionAdded(Emotion emotion) {
		loadPublicEmotions();
		loadPrivateEmotions();
		for (Iterator iterator = EMOTION_LISTENERS.iterator(); iterator.hasNext();) {
			try {
				EmotionListener emotionListener = (EmotionListener) iterator.next();
				emotionListener.emotionAdded(emotion);
			} catch (Exception e) {
			}
		}
	}

	/**
	 * <br>2013-9-7 下午3:01:38
	 * @see com.xiongyingqi.emotion.EmotionListener#emotionUpdated(com.xiongyingqi.emotion.Emotion)
	 */
	@Override
	public void emotionUpdated(Emotion emotion) {
		for (Iterator iterator = EMOTION_LISTENERS.iterator(); iterator.hasNext();) {
			try {
				EmotionListener emotionListener = (EmotionListener) iterator.next();
				emotionListener.emotionUpdated(emotion);
			} catch (Exception e) {
			}
		}
	}

	/**
	 * <br>2013-9-7 下午3:02:26
	 * @see com.xiongyingqi.emotion.EmotionListener#emotionDeleted(com.xiongyingqi.emotion.Emotion)
	 */
	@Override
	public void emotionDeleted(Emotion emotion) {
		for (Iterator iterator = EMOTION_LISTENERS.iterator(); iterator.hasNext();) {
			try {
				EmotionListener emotionListener = (EmotionListener) iterator.next();
				emotionListener.emotionDeleted(emotion);
			} catch (Exception e) {
			}
		}
	}

	/**
	 * Set<Emotion>
	 * @return the publicEmotions
	 */
	public Set<Emotion> getPublicEmotions() {
		if(publicEmotions.size() == 0){
			loadPublicEmotions();
		}
		return publicEmotions;
	}

	/**
	 * Set<Emotion>
	 * @param publicEmotions the publicEmotions to set
	 */
	public void setPublicEmotions(Set<Emotion> publicEmotions) {
		this.publicEmotions = publicEmotions;
	}

	/**
	 * Set<Emotion>
	 * @return the privatEmotions
	 */
	public Set<Emotion> getPrivatEmotions() {
		if(privatEmotions.size() == 0){
			loadPrivateEmotions();
		}
		return privatEmotions;
	}

	/**
	 * Set<Emotion>
	 * @param privatEmotions the privatEmotions to set
	 */
	public void setPrivatEmotions(Set<Emotion> privatEmotions) {
		this.privatEmotions = privatEmotions;
	}
	
	/**
	 * 根据表情id获取表情对象
	 * <br>2013-9-18 下午2:46:26
	 * @param id 服务器的表情标识
	 * @return
	 */
	public Emotion getEmotionById(String id){
		Emotion emotion = new Emotion();
		emotion.setId(id);
		if(publicEmotions.contains(emotion)){
			for (Iterator<Emotion> iterator = publicEmotions.iterator(); iterator.hasNext();) {
				Emotion emotion2 = iterator.next();
				if(emotion2.equals(emotion)){
					return emotion2;
				}
			}
		}
		return null;
	}

	/**
	 * 根据emotionId获取emotion
	 * <br>2013-9-22 上午10:48:44
	 * @param emotionId
	 * @return
	 */
	public Emotion getEmotionByEmotionId(String emotionId) {
		Emotion emotion = new Emotion();
		emotion.setEmotionId(emotionId);
		Set<Emotion> emotions = new HashSet<Emotion>();
		emotions.addAll(privatEmotions);
		emotions.addAll(publicEmotions);
		if(emotions.contains(emotion)){
			for (Iterator<Emotion> iterator = emotions.iterator(); iterator.hasNext();) {
				Emotion emotion2 = iterator.next();
				if(emotion2.equals(emotion)){
					return emotion2;
				}
			}
		}
		return null;
	}
	
}
