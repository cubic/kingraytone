/**
 * spark_src
 */
package com.xiongyingqi.emotion;

import java.awt.Image;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.Icon;

import org.dom4j.Element;

import com.kingray.spark.packet.IQElementMapping;
import com.xiongyingqi.convert.XmlConvert;
import com.xiongyingqi.utils.Base64;
import com.xiongyingqi.utils.EntityHelper;
import com.xiongyingqi.utils.FileHelper;
import com.xiongyingqi.utils.StringHelper;
import com.xiongyingqi.vo.VO;

/**
 * 表情对象
 * @author 瑛琪 <a href="http://xiongyingqi.com">xiongyingqi.com</a>
 * @version 2013-9-4 下午4:24:29
 */
@IQElementMapping("emotion")
public class Emotion implements VO{

	/**
	 * 2013-9-22 下午2:16:09
	 * long Emotion.java
	 */
	private static final long serialVersionUID = -8743807355140689225L;

	/**
	 * 序列化表情对象为文件的后缀名
	 */
	public static final String EMOTION_IMAGE_FILE_SUFFIX = ".emotion";
	
	/**
	 * 序列化表情对象为文件的后缀名
	 */
	public static final String SCALED_EMOTION_IMAGE_FILE_SUFFIX = ".scaleemotion";
	/**
	 * 存储表情图标的文件后缀名
	 */
	public static final String EMOTION_INFO_FILE_SUFFIX = ".info";
	
	/**
	 * 图标所指向的文件
	 */
	private File emotionFile;
	
	private File scaledEmotionFile;
	
	/**
	 * 图标的显示字符
	 */
	@IQElementMapping("emotionString")
	private String emotionString;
	
	/**
	 * 服务器的id
	 */
	@IQElementMapping("id")
	private String id;
	
	/**
	 * 系统全局标识
	 */
	@IQElementMapping("emotionId")
	private String emotionId;
	
	/**
	 * 图标是否私有
	 */
	private boolean isPrivate;
	
	/**
	 * 将图标转换为Base64字符串
	 * <br>2013-9-4 下午4:33:22
	 * @return
	 */
	@IQElementMapping("base64Data")
	public String toBase64(){
		String base64 = null;
		try {
			if(emotionFile == null){
				return null;
			}
			byte[] data = FileHelper.readFileToBytes(emotionFile);
			base64 = Base64.encodeBytes(data);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return base64;
	}
	
	
	/**
	 * File
	 * @return the scaledEmotionFile
	 */
	public File getScaledEmotionFile() {
		synchronized (EMOTION_IMAGE_FILE_SUFFIX) {
			if(scaledEmotionFile == null){
				String emotionFilePath = getEmotionFile().getAbsolutePath();
				int index = emotionFilePath.lastIndexOf(EMOTION_IMAGE_FILE_SUFFIX);
				emotionFilePath = emotionFilePath.substring(0, index);
				String scaledEmotionFilePath = emotionFilePath + SCALED_EMOTION_IMAGE_FILE_SUFFIX;
				scaledEmotionFile = new File(scaledEmotionFilePath);
			}
		}
		return scaledEmotionFile;
	}


	/**
	 * 将Base64字符串转换为公共表情文件
	 * <br>2013-9-6 下午3:23:30
	 * @param base64Data
	 * @param emotionId
	 * @return
	 */
	public static File convertToPublicEmotionFile(String base64Data, String emotionId){
		return convertToFile(base64Data, emotionId, EmotionManager.EMOTION_FOLDER);
	}
	
	/**
	 * 将Base64字符串转换为私有表情文件
	 * <br>2013-9-6 下午3:23:30
	 * @param base64Data
	 * @param emotionId
	 * @return
	 */
	public static File convertToPrivateEmotionFile(String base64Data, String emotionId){
		return convertToFile(base64Data, emotionId, EmotionManager.EMOTION_PRIVATE_FOLDER);
	}
	
	
	
	/**
	 * 将Base64字符串转换为文件
	 * <br>2013-9-22 上午10:12:21
	 * @param base64Data
	 * @param emotionId
	 * @param folder
	 * @return
	 */
	public static File convertToFile(String base64Data, String emotionId, File folder){
		File file = null;
		if(base64Data != null && !"".equals(base64Data.trim())){
			byte[] data = Base64.decode(base64Data);
			try {
				file = FileHelper.readBytesToFile(data, FileHelper.validateFile(new File(folder, emotionId + EMOTION_IMAGE_FILE_SUFFIX)));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return file;
	}
	
	
	/**
	 * 以id作为唯一标识
	 * <br>2013-9-5 下午5:51:15
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		if(id == null){
			return emotionId.hashCode();
		}
		return id.hashCode();
	}
	
	/**
	 * 重写equals，方便容器的比较
	 * <br>2013-9-5 下午5:51:51
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(obj != null && obj instanceof Emotion && obj.hashCode() == this.hashCode()){
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * File
	 * @return the emotionFile
	 */
	public File getEmotionFile() {
		return emotionFile;
	}
	/**
	 * File
	 * @param emotionFile the emotionFile to set
	 */
	public void setEmotionFile(File emotionFile) {
		this.emotionFile = emotionFile;
	}
	/**
	 * String
	 * @return the emotionString
	 */
	public String getEmotionString() {
		return emotionString;
	}
	/**
	 * String
	 * @param emotionString the emotionString to set
	 */
	public void setEmotionString(String emotionString) {
		this.emotionString = emotionString;
	}
	
	/**
	 * String
	 * @return the emotionId
	 */
	public String getEmotionId() {
		return emotionId;
	}

	/**
	 * String
	 * @param emotionId the emotionId to set
	 */
	public void setEmotionId(String emotionId) {
		this.emotionId = emotionId;
	}

	/**
	 * boolean
	 * @return the isPrivate
	 */
	public boolean isPrivate() {
		return isPrivate;
	}
	/**
	 * boolean
	 * @param isPrivate the isPrivate to set
	 */
	public void setPrivate(boolean isPrivate) {
		this.isPrivate = isPrivate;
	}
	
	/**
	 * 存储Emotion信息，此操作是将Emotion的信息写入文件<br>
	 * <b>注意：在调用该方法时一定要确定所有Emotion的必要信息都设置完成</b>
	 * <br>2013-9-6 下午12:04:30
	 */
	public void store() throws Exception{
		if(emotionFile == null){
			throw new Exception("表情文件为空！");
		}
		File folder = emotionFile.getParentFile();
		File file = new File(folder, emotionId + EMOTION_INFO_FILE_SUFFIX);
		if(file.exists()){
			file.delete();
		}
		FileHelper.serializeObjectToFile(file, this);
	}
	
	public static String nextEmotionFileName(){
		String fileName = StringHelper.UUID() + EMOTION_IMAGE_FILE_SUFFIX;
		return fileName;
	}
	
	/**
	 * String
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * String
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * <br>2013-9-22 上午11:33:25
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return EntityHelper.reflectToString(this);
	}

	public static void main(String[] args) {
		XmlConvert convert  = new XmlConvert(IQElementMapping.class);
		Emotion emotion = new Emotion();
		emotion.setEmotionString("aaa");
		emotion.setEmotionId("111");
		emotion.setEmotionFile(null);
		
		try {
			emotion.store();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Collection<Emotion> emotions = new ArrayList<Emotion>();
		emotions.add(emotion);
		Element element = convert.convertVOs(emotions);
		System.out.println(element.asXML());
	}
	
}
