import java.io.File;       
import java.util.ArrayList;       
import java.util.List;       
/**    
 * MyEclipse9 插件配置代码生成器 
 * 
 * 一、到官方上下载svn1.8.3，下载后的文件名叫site-1.8.3.zip
地址：http://subclipse.tigris.org/servlets/ProjectDocumentList?folderID=2240 这里有很多版本，请自己选择合适的；
二、解压，删除解压后得到的文件中除了名叫features和plugins的两个文件夹以外的其他所有文件。
方法1：
   a) 在MyEclipse10.0的安装目录中自己新建一个文件夹（我的叫MyPlugins）然后把解压并删除过文件后得到的那个SVN文件夹复制到该目录；
   b)在MyEclipse10.0的安装目录下的dropins文件夹中建立一个文本文件，内容输path=你的svn路径，（我的是path=E:\\MyEclipse\\MyEclipse 10\\MyPlugins\\SVN-1.8.3），然后保存，保存后将该文件名重命名成SVN.lnk(记住扩展名是是.lnk不是网上有网友说的link).
   c)接下来删除 MyEclipse10.0的安装目录下configuration文件夹中的org.eclipse.update文件夹。
    d)重启－〉OK。
方法二：
    a)features和plugins的两个文件中的内容分别放到MyEclipse10.0的安装目录下的common目录下的features 和plugins中。
    b)重启MyEclipse 10.0
方法三：在线安装
  a)打开 http://subclipse.tigris.org/servlets/ProjectProcess?pageID=p4wYuA 找到最新的发布 Links for 1.8.x Release 下面有：Eclipse update site URL ：Eclipse update site URL: http://subclipse.tigris.org/update_1.8.x

 b) 打开MyEclipse 10.0 点Help->MyEclipse Configuration Center,再点击Software选项卡，在左边的搜索插件输入框右边，有Add Site链接，点击
添加Name 和 URL



添加后，可以在下面的Personal Sites里看到，展开，双击要安装的项，右边的Pending Changes框内会有 Apply XXX Changes按钮，点击即可安装。


 c)重启MyEclipse10.0 ,安装完成。
 
方法四：
直接将插件解压后复制到MyEclipse 10\dropins目录下，然后打开MyEclipse 10，就可以看到插件已经可以使用了。

注意：插件的plugins目录和features目录必须放在dropins目录的下一级子目录（最好以插件名称、版本命名，可以更好的管理插件）下。

目录结构如：（以SVN为例）
dropins
|
 -- subclipse-site-1.6.10
 |
    -- features
   |
   -- plugins
 */      
public class PluginConfigCreator       
{       
    public PluginConfigCreator()       
    {       
    }       
    public void print(String path)       
    {       
        List<String> list = getFileList(path);       
        if (list == null)       
        {       
            return;       
        }       
        int length = list.size();       
        for (int i = 0; i < length; i++)       
        {       
            String result = "";       
            String thePath = getFormatPath(getString(list.get(i)));       
            File file = new File(thePath);       
            if (file.isDirectory())       
            {       
                String fileName = file.getName();       
                if (fileName.indexOf("_") < 0)       
                {       
                    print(thePath);       
                    continue;       
                }       
                String[] filenames = fileName.split("_");       
                String filename1 = filenames[0];       
                String filename2 = filenames[1];       
                result = filename1 + "," + filename2 + ",file:/" + path + "/"      
                        + fileName + "\\\\,4,false";       
                System.out.println(result);       
            } else if (file.isFile())       
            {       
                String fileName = file.getName();       
                if (fileName.indexOf("_") < 0)       
                {       
                    continue;       
                }       
                int last = fileName.lastIndexOf("_");// 最后一个下划线的位置       
                String filename1 = fileName.substring(0, last);       
                String filename2 = fileName.substring(last + 1, fileName       
                        .length() - 4);       
                result = filename1 + "," + filename2 + ",file:/" + path + "/"      
                        + fileName + ",4,false";       
                System.out.println(result);       
            }       
        }       
    }       
    public List<String> getFileList(String path)       
    {       
        path = getFormatPath(path);       
        path = path + "/";       
        File filePath = new File(path);       
        if (!filePath.isDirectory())       
        {       
            return null;       
        }       
        String[] filelist = filePath.list();       
        List<String> filelistFilter = new ArrayList<String>();       
        for (int i = 0; i < filelist.length; i++)       
        {       
            String tempfilename = getFormatPath(path + filelist[i]);       
            filelistFilter.add(tempfilename);       
        }       
        return filelistFilter;       
    }       
    public String getString(Object object)       
    {       
        if (object == null)       
        {       
            return "";
        }       
        return String.valueOf(object);       
    }       
    public String getFormatPath(String path)       
    {       
        path = path.replaceAll("\\\\\\\\", "/");
        path = path.replaceAll("//", "/");
        return path;
    }       
    public static void main(String[] args)       
    {       
        /*你的SVN的features 和 plugins复制后放的目录*/      
        String plugin = "D:\\software\\MyEclipse10.0\\myplugin\\visual editor 1.5";       
        new PluginConfigCreator().print(plugin);       
    }       
}  